import java.util.Scanner;

class Fahrkartenautomat {
    public static void main(String[] args) {
        long zuZahlenderBetrag;
        long rueckgabebetrag;

        zuZahlenderBetrag = fahrkartenbestellungErfassen();
        rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(rueckgabebetrag);
    }

    public static long fahrkartenbestellungErfassen() {
        Scanner tastatur = new Scanner(System.in);

        byte anzahlTickets;
        long zuZahlenderBetrag;

        System.out.print("Ticketpreis (EURO): ");
        zuZahlenderBetrag = (long)(tastatur.nextFloat() * 100);

        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextByte();

        return zuZahlenderBetrag * anzahlTickets;
    }

    public static long fahrkartenBezahlen(long zuZahlen) {
        Scanner tastatur = new Scanner(System.in);
        long eingezahlterGesamtbetrag = 0;

        while (eingezahlterGesamtbetrag < zuZahlen) {
            System.out.printf("Noch zu zahlen: %.2f Euro\n", (float)(zuZahlen - eingezahlterGesamtbetrag) / 100);
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            long eingeworfeneMuenze = (long) (tastatur.nextFloat() * 100);
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }

        return eingezahlterGesamtbetrag - zuZahlen;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein/e wird/werden ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            warte(250);
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(long rueckgabebetrag) {
        if (rueckgabebetrag > 0.0) {
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", (float)rueckgabebetrag / 100);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rueckgabebetrag >= 200) // 2 EURO-Münzen
            {
                muenzeAusgeben(2, "Euro");
                rueckgabebetrag -= 200;
            }
            while (rueckgabebetrag >= 100) // 1 EURO-Münzen
            {
                muenzeAusgeben(1, "Euro");
                rueckgabebetrag -= 100;
            }
            while (rueckgabebetrag >= 50) // 50 CENT-Münzen
            {
                muenzeAusgeben(50, "CENT");
                rueckgabebetrag -= 50;
            }
            while (rueckgabebetrag >= 20) // 20 CENT-Münzen
            {
                muenzeAusgeben(20, "CENT");
                rueckgabebetrag -= 20;
            }
            while (rueckgabebetrag >= 10) // 10 CENT-Münzen
            {
                muenzeAusgeben(10, "CENT");
                rueckgabebetrag -= 10;
            }
            while (rueckgabebetrag >= 5)// 5 CENT-Münzen
            {
                muenzeAusgeben(5, "CENT");
                rueckgabebetrag -= 5;
            }
        }
        System.out.println("""
                
                Vergessen Sie nicht, den Fahrschein
                vor Fahrtantritt entwerten zu lassen!
                Wir wünschen Ihnen eine gute Fahrt.""");
    }

    public static void warte(int millisekunde) {
        try {
            Thread.sleep(millisekunde);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void muenzeAusgeben(long betrag, String einheit) {
        System.out.printf("%d %s\n", betrag, einheit);
    }
}

// 2.5: 5
// Ich habe den Datentyp BYTE gewählt, da eine maximale Ticketanzahl von 127 ausreichen sollte (niemand kauft mehr als 127 Tickets).

// 2.5: 6
// Der Wert, welcher sich aus der Multiplikation ergibt, wird der bestehenden Variable zugewiesen.
