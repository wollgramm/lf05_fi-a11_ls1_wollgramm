public class Mathe {
    public static double quadrat(double input) {
        return Math.sqrt(input);
    }
    public static double hypotenuse(double kathete1, double kathete2) {
        return Mathe.quadrat(quadrat(kathete1) + quadrat(kathete2));
    }
}
