import java.util.Scanner;

public class Fahrsimulator {
    public static void main(String[] args) {
        double velocity = 0;
        Scanner scanner = new Scanner(System.in);
        printGreeter();

        while (true) {
            switch (eingabe(scanner)) {
                case 'a' -> {
                    System.out.println("Auf Wiedersehen!");
                    System.exit(0);
                }
                case 'w' -> velocity = beschleunige(velocity, 10.0);
                case 's' -> velocity = beschleunige(velocity, -20.0);
                default -> System.out.println("Falsche Eingabe");
            }
            System.out.printf("Du fährst %.2f km/h!\n", velocity);
        }

    }

    public static void printGreeter() {
        System.out.println("Willkommen im Fahrsimmulator!");
        System.out.println("-----------------------------");
    }

    public static char eingabe(Scanner scanner) {
        System.out.println("Was möchtest du machen?\n w = beschleunigen\n s = bremsen\n a = aussteigen\n\n");
        return scanner.next().charAt(0);
    }

    public static double beschleunige(double v, double dv) {
        v=v+dv;
        if (v > 130) {
            v = 130;
        } else if (v < 0) {
            v = 0;
        }

        return v;
    }
}
