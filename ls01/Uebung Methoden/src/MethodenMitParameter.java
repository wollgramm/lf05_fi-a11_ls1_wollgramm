public class MethodenMitParameter {
    public static void main(String[] args) {

    }

    double reihenschaltung(double r1, double r2) {
        return r1 + r2;
    }

    double parallelschaltung(double r1, double r2) {
        return (r1*r2)/(r1+r2);
    }
}
