import java.util.Scanner;

public class Quadrieren {

	public static void main(String[] args) {
		printGreeter();
		double input = readDouble();
		double ergebnis = square(input);
		printResult(input, ergebnis);
	}

	private static void printGreeter() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x²");
		System.out.println("---------------------------------------------");
	}

	private static double readDouble() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Welche Zahl soll ich quadrieren? ");
		return scanner.nextDouble();
	}

	private static double square(double input) {
		return input * input;
	}

	private static void printResult(double input, double result) {
		System.out.printf("\n\nx = %.2f und x²= %.2f\n", input, result);
	}
}
