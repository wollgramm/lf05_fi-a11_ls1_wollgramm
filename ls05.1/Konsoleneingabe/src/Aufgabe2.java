import java.util.Scanner;

public class Aufgabe2 {

    public static void main(String[] args) {
        String name;
        int alter;
        float groesse;
        char geschlecht;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Geben Sie einen Namen ein: ");
        name = scanner.next();

        System.out.print("Geben Sie Ihr Alter ein: ");
        alter = scanner.nextInt();

        System.out.print("Geben Sie Ihre Größe in Meter ein: ");
        groesse = scanner.nextFloat();

        System.out.print("Geben Sie Ihr Geschlecht ein (w/m/d): ");
        geschlecht = scanner.next().charAt(0);

        System.out.printf("\nDu heißt %s, bist %d Jahre alt und du bist %.2f Meter groß.\nDein Geschlecht ist %c.", name, alter, groesse, geschlecht);
    }
}
