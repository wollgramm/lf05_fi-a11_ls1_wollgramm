package com.company;

public class WeltDerZahlen {

    public static void main(String[] args) {

        /*  *********************************************************

         Zuerst werden die Variablen mit den Werten festgelegt!

        *********************************************************** */
        // Im Internet gefunden ?
        // Die Anzahl der Planeten in unserem Sonnesystem
        byte anzahlPlaneten = 9;

        // Anzahl der Sterne in unserer Milchstraße
        long anzahlSterne = 100_000_000_000L;

        // Wie viele Einwohner hat Berlin?
        int bewohnerBerlin = 3_664_088;

        // Wie alt bist du?  Wie viele Tage sind das?

        short alterTage = 21;

        // Wie viel wiegt das schwerste Tier der Welt?
        // Schreiben Sie das Gewicht in Kilogramm auf!
        int gewichtKilogramm = 200_000;

        // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
        int flaecheGroessteLand = 17_098_242;

        // Wie groß ist das kleinste Land der Erde?

        float flaecheKleinsteLand = 0.44f;

    /*  *********************************************************

         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen

    *********************************************************** */

        System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);

        System.out.println("Anzahl der Sterne: " + anzahlSterne);

        System.out.println("Anzahl Bewohner Berlins " + bewohnerBerlin);
        System.out.println("Mein Alter in Tagen: " + alterTage);
        System.out.println("Gewicht schwerstes Tier: " + gewichtKilogramm);
        System.out.println("Fläche größtes Land: " + flaecheGroessteLand);
        System.out.println("Fläche kleinstes Land: " + flaecheKleinsteLand);




                System.out.println(" *******  Ende des Programms  ******* ");

    }
}
