package com.company;

public class Variablen {

    public static void main(String [] args){
        /* 1. Ein Zähler soll die Programmdurchläufe zählen.
              Vereinbaren Sie eine geeignete Variable */
        short programmdurchlauf;

        /* 2. Weisen Sie dem Zähler den Wert 25 zu
              und geben Sie ihn auf dem Bildschirm aus.*/
        programmdurchlauf = 25;
        System.out.println(programmdurchlauf);

        /* 3. Durch die Eingabe eines Buchstabens soll der Menüpunkt
              eines Programms ausgewählt werden.
              Vereinbaren Sie eine geeignete Variable */
        char selectedMenu;

        /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
              und geben Sie ihn auf dem Bildschirm aus.*/
        selectedMenu = 'C';
        System.out.println(selectedMenu);

        /* 5. Für eine genaue astronomische Berechnung sind grosse Zahlenwerte
              notwendig.
              Vereinbaren Sie eine geeignete Variable */
       double calc;

        /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
              und geben Sie sie auf dem Bildschirm aus.*/
        calc = 300_000;
        System.out.println(calc);

        /* 7. Sieben Personen gründen einen Verein. Für eine Vereinsverwaltung
              soll die Anzahl der Mitglieder erfasst werden.
              Vereinbaren Sie eine geeignete Variable und initialisieren sie
              diese sinnvoll.*/
        byte members = 7;

        /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
        System.out.println(members);

        /* 9. Für eine Berechnung wird die elektrische Elementarladung benötigt.
              Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
              dem Bildschirm aus.*/
        final double elementaryCharge = 1.602E-19;
        System.out.println(elementaryCharge);

        /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
              Vereinbaren Sie eine geeignete Variable. */
        boolean paymentSuccessful;

        /*11. Die Zahlung ist erfolgt.
              Weisen Sie der Variable den entsprechenden Wert zu
              und geben Sie die Variable auf dem Bildschirm aus.*/
        paymentSuccessful = true;
        System.out.println(paymentSuccessful);

    }//main
}// Variablen
