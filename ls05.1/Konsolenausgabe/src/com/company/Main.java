package com.company;

public class Main {

    public static void main(String[] args) {
        /*
         *  Aufgabe 1
         */
        System.out.print("Das ist nicht mein genialer \"Beispielsatz\" ... " + "\n");
        System.out.print("Mein \"genialer\" Beispielsatz ist das.\n");

        // print() gibt einen Text aus
        // println() gibt einen Text aus und führt einen Zeilenumbruch aus

        /*
         *  Aufgabe 2
         */
        String stern = "*";
        String stern_hälfte = "";

        System.out.printf("%6s*%-6s\n", stern_hälfte, stern_hälfte);
        stern_hälfte = stern_hälfte + stern;
        System.out.printf("%6s*%-6s\n", stern_hälfte, stern_hälfte);
        stern_hälfte = stern_hälfte + stern;
        System.out.printf("%6s*%-6s\n", stern_hälfte, stern_hälfte);
        stern_hälfte = stern_hälfte + stern;
        System.out.printf("%6s*%-6s\n", stern_hälfte, stern_hälfte);
        stern_hälfte = stern_hälfte + stern;
        System.out.printf("%6s*%-6s\n", stern_hälfte, stern_hälfte);
        stern_hälfte = stern_hälfte + stern;
        System.out.printf("%6s*%-6s\n", stern_hälfte, stern_hälfte);
        stern_hälfte = stern_hälfte + stern;
        System.out.printf("%6s*%-6s\n", stern_hälfte, stern_hälfte);

        System.out.printf("%8s\n", "***");
        System.out.printf("%8s\n", "***");

        /*
         *  Aufgabe 3
         */
        System.out.printf("%.2f\n", 22.4234234);
        System.out.printf("%.2f\n", 11.2222);
        System.out.printf("%.2f\n", 4.0);
        System.out.printf("%.2f\n", 1000000.551);
        System.out.printf("%.2f\n", 97.34);
    }
}
