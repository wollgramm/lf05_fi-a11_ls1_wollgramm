package com.company;

public class Aufgabe_2 {
    public static void main(String[] args) {
        int n = 0;
        String rechnung = " ";
        int ergebnis = 1;

        // 0!
        System.out.printf("%d%-4s", n, "!");
        System.out.print("=");
        System.out.printf("%-19s", rechnung);
        System.out.print("=");
        System.out.printf("%4s", ergebnis);
        System.out.println();

        // 1!
        n = n + 1;
        ergebnis = ergebnis * n;
        rechnung = rechnung + n;
        System.out.printf("%d%-4s", n, "!");
        System.out.print("=");
        System.out.printf("%-19s", rechnung);
        System.out.print("=");
        System.out.printf("%4s", ergebnis);
        System.out.println();

        // 2!
        n = 2;
        ergebnis = ergebnis * n;
        rechnung = rechnung + " * " + n;
        System.out.printf("%d%-4s", n, "!");
        System.out.print("=");
        System.out.printf("%-19s", rechnung);
        System.out.print("=");
        System.out.printf("%4s", ergebnis);
        System.out.println();

        // 3!
        n = 3;
        ergebnis = ergebnis * n;
        rechnung = rechnung + " * " + n;
        System.out.printf("%d%-4s", n, "!");
        System.out.print("=");
        System.out.printf("%-19s", rechnung);
        System.out.print("=");
        System.out.printf("%4s", ergebnis);
        System.out.println();

        // 4!
        n = 4;
        ergebnis = ergebnis * n;
        rechnung = rechnung + " * " + n;
        System.out.printf("%d%-4s", n, "!");
        System.out.print("=");
        System.out.printf("%-19s", rechnung);
        System.out.print("=");
        System.out.printf("%4s", ergebnis);
        System.out.println();

        // 5!
        n = 5;
        ergebnis = ergebnis * n;
        rechnung = rechnung + " * " + n;
        System.out.printf("%d%-4s", n, "!");
        System.out.print("=");
        System.out.printf("%-19s", rechnung);
        System.out.print("=");
        System.out.printf("%4s", ergebnis);
        System.out.println();
    }
}
